
           EXEC SQL DECLARE SSD_SYSDAT_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           UNVRS_CD            CHAR(2) NOT NULL WITH DEFAULT,
           FIMS_ENTY_CD        CHAR(8) NOT NULL,
           ELMNT_NAME          CHAR(30) NOT NULL,
           COA_CD              CHAR(1) NOT NULL WITH DEFAULT,
           ACTVY_DT            DATE NOT NULL WITH DEFAULT)
           END-EXEC.
       01  SSD-SSD-SYSDAT.
           05  SSD-USER-CD                    PIC X(08).
           05  SSD-LAST-ACTVY-DT              PIC X(05).
           05  SSD-UNVRS-CD                   PIC X(02).
           05  SSD-FIMS-ENTY-CD               PIC X(08).
           05  SSD-ELMNT-NAME                 PIC X(30).
           05  SSD-COA-CD                     PIC X(01).
           05  SSD-ACTVY-DT                   PIC X(10).
