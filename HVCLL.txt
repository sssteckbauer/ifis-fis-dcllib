
           EXEC SQL DECLARE CLL_COMM_LDTL_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           START_DT            DATE NOT NULL WITH DEFAULT,
           FK_CLG_COMM_REFNBR  DECIMAL(6) NOT NULL,
           TEXT_CD             CHAR(4) NOT NULL WITH DEFAULT,
           OFC_CD              CHAR(4) NOT NULL WITH DEFAULT,
           COMM_NAME           CHAR(35) NOT NULL,
           COMM_ADR_TYP        CHAR(2) NOT NULL WITH DEFAULT,
           COMM_VARIABLE       CHAR(15) NOT NULL WITH DEFAULT,
           PKEY_TS             TIMESTAMP NOT NULL,
           FK_CLG_TXT_CD       CHAR(4) NOT NULL WITH DEFAULT,
           CLG_SET_TS          TIMESTAMP NOT NULL WITH DEFAULT,
           PRS_SETF            CHAR(1) NOT NULL WITH DEFAULT,
           FK_PRS_IREF_ID      DECIMAL(7) NOT NULL WITH DEFAULT,
           PRS_SET_TS          TIMESTAMP NOT NULL WITH DEFAULT,
           ENT_SETF            CHAR(1) NOT NULL WITH DEFAULT,
           FK_ENT_ENTY_ONE     CHAR(1) NOT NULL WITH DEFAULT,
           FK_ENT_ENTY_NINE    CHAR(9) NOT NULL WITH DEFAULT,
           ENT_SET_TS          TIMESTAMP NOT NULL WITH DEFAULT)
           END-EXEC.
       01  CLL-CLL-COMM-LDTL.
           05  CLL-USER-CD                    PIC X(08).
           05  CLL-LAST-ACTVY-DT              PIC X(05).
           05  CLL-START-DT                   PIC X(10).
           05  CLL-FK-CLG-COMM-REFNBR         PIC S9(06)         COMP-3.
           05  CLL-TEXT-CD                    PIC X(04).
           05  CLL-OFC-CD                     PIC X(04).
           05  CLL-COMM-NAME                  PIC X(35).
           05  CLL-COMM-ADR-TYP               PIC X(02).
           05  CLL-COMM-VARIABLE              PIC X(15).
           05  CLL-PKEY-TS                    PIC X(26).
           05  CLL-FK-CLG-TXT-CD              PIC X(04).
           05  CLL-CLG-SET-TS                 PIC X(26).
           05  CLL-PRS-SETF                   PIC X(1).
           05  CLL-FK-PRS-IREF-ID             PIC S9(07)         COMP-3.
           05  CLL-PRS-SET-TS                 PIC X(26).
           05  CLL-ENT-SETF                   PIC X(1).
           05  CLL-FK-ENT-ENTY-ONE            PIC X(01).
           05  CLL-FK-ENT-ENTY-NINE           PIC X(09).
           05  CLL-ENT-SET-TS                 PIC X(26).
