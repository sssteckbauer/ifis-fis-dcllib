
           EXEC SQL DECLARE FLY_FSCL_YEAR_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           UNVRS_CD            CHAR(2) NOT NULL WITH DEFAULT,
           COA_CD              CHAR(1) NOT NULL WITH DEFAULT,
           FSCL_YR             CHAR(2) NOT NULL,
           ACTVY_DT            DATE NOT NULL WITH DEFAULT,
           NBR_OF_PRDS         DECIMAL(2) NOT NULL WITH DEFAULT,
           FSCL_YR_START_DT    DATE NOT NULL WITH DEFAULT,
           FSCL_YR_END_DT      DATE NOT NULL WITH DEFAULT,
           PRD_STATUS_01       CHAR(1) NOT NULL WITH DEFAULT,
           PRD_STATUS_02       CHAR(1) NOT NULL WITH DEFAULT,
           PRD_STATUS_03       CHAR(1) NOT NULL WITH DEFAULT,
           PRD_STATUS_04       CHAR(1) NOT NULL WITH DEFAULT,
           PRD_STATUS_05       CHAR(1) NOT NULL WITH DEFAULT,
           PRD_STATUS_06       CHAR(1) NOT NULL WITH DEFAULT,
           PRD_STATUS_07       CHAR(1) NOT NULL WITH DEFAULT,
           PRD_STATUS_08       CHAR(1) NOT NULL WITH DEFAULT,
           PRD_STATUS_09       CHAR(1) NOT NULL WITH DEFAULT,
           PRD_STATUS_10       CHAR(1) NOT NULL WITH DEFAULT,
           PRD_STATUS_11       CHAR(1) NOT NULL WITH DEFAULT,
           PRD_STATUS_12       CHAR(1) NOT NULL WITH DEFAULT,
           PRD_STATUS_13       CHAR(1) NOT NULL WITH DEFAULT,
           PRD_START_DT_01     DATE NOT NULL WITH DEFAULT,
           PRD_START_DT_02     DATE NOT NULL WITH DEFAULT,
           PRD_START_DT_03     DATE NOT NULL WITH DEFAULT,
           PRD_START_DT_04     DATE NOT NULL WITH DEFAULT,
           PRD_START_DT_05     DATE NOT NULL WITH DEFAULT,
           PRD_START_DT_06     DATE NOT NULL WITH DEFAULT,
           PRD_START_DT_07     DATE NOT NULL WITH DEFAULT,
           PRD_START_DT_08     DATE NOT NULL WITH DEFAULT,
           PRD_START_DT_09     DATE NOT NULL WITH DEFAULT,
           PRD_START_DT_10     DATE NOT NULL WITH DEFAULT,
           PRD_START_DT_11     DATE NOT NULL WITH DEFAULT,
           PRD_START_DT_12     DATE NOT NULL WITH DEFAULT,
           PRD_START_DT_13     DATE NOT NULL WITH DEFAULT,
           PRD_END_DT_01       DATE NOT NULL WITH DEFAULT,
           PRD_END_DT_02       DATE NOT NULL WITH DEFAULT,
           PRD_END_DT_03       DATE NOT NULL WITH DEFAULT,
           PRD_END_DT_04       DATE NOT NULL WITH DEFAULT,
           PRD_END_DT_05       DATE NOT NULL WITH DEFAULT,
           PRD_END_DT_06       DATE NOT NULL WITH DEFAULT,
           PRD_END_DT_07       DATE NOT NULL WITH DEFAULT,
           PRD_END_DT_08       DATE NOT NULL WITH DEFAULT,
           PRD_END_DT_09       DATE NOT NULL WITH DEFAULT,
           PRD_END_DT_10       DATE NOT NULL WITH DEFAULT,
           PRD_END_DT_11       DATE NOT NULL WITH DEFAULT,
           PRD_END_DT_12       DATE NOT NULL WITH DEFAULT,
           PRD_END_DT_13       DATE NOT NULL WITH DEFAULT,
           END_OF_QTR_IND_01   CHAR(1) NOT NULL WITH DEFAULT,
           END_OF_QTR_IND_02   CHAR(1) NOT NULL WITH DEFAULT,
           END_OF_QTR_IND_03   CHAR(1) NOT NULL WITH DEFAULT,
           END_OF_QTR_IND_04   CHAR(1) NOT NULL WITH DEFAULT,
           END_OF_QTR_IND_05   CHAR(1) NOT NULL WITH DEFAULT,
           END_OF_QTR_IND_06   CHAR(1) NOT NULL WITH DEFAULT,
           END_OF_QTR_IND_07   CHAR(1) NOT NULL WITH DEFAULT,
           END_OF_QTR_IND_08   CHAR(1) NOT NULL WITH DEFAULT,
           END_OF_QTR_IND_09   CHAR(1) NOT NULL WITH DEFAULT,
           END_OF_QTR_IND_10   CHAR(1) NOT NULL WITH DEFAULT,
           END_OF_QTR_IND_11   CHAR(1) NOT NULL WITH DEFAULT,
           END_OF_QTR_IND_12   CHAR(1) NOT NULL WITH DEFAULT,
           END_OF_QTR_IND_13   CHAR(1) NOT NULL WITH DEFAULT,
           ACRL_PRD_STATUS     CHAR(1) NOT NULL WITH DEFAULT,
           BEG_PURGE_FLAG      CHAR(1) NOT NULL WITH DEFAULT,
           PRD_PRG_FLAG_01     CHAR(1) NOT NULL WITH DEFAULT,
           PRD_PRG_FLAG_02     CHAR(1) NOT NULL WITH DEFAULT,
           PRD_PRG_FLAG_03     CHAR(1) NOT NULL WITH DEFAULT,
           PRD_PRG_FLAG_04     CHAR(1) NOT NULL WITH DEFAULT,
           PRD_PRG_FLAG_05     CHAR(1) NOT NULL WITH DEFAULT,
           PRD_PRG_FLAG_06     CHAR(1) NOT NULL WITH DEFAULT,
           PRD_PRG_FLAG_07     CHAR(1) NOT NULL WITH DEFAULT,
           PRD_PRG_FLAG_08     CHAR(1) NOT NULL WITH DEFAULT,
           PRD_PRG_FLAG_09     CHAR(1) NOT NULL WITH DEFAULT,
           PRD_PRG_FLAG_10     CHAR(1) NOT NULL WITH DEFAULT,
           PRD_PRG_FLAG_11     CHAR(1) NOT NULL WITH DEFAULT,
           PRD_PRG_FLAG_12     CHAR(1) NOT NULL WITH DEFAULT,
           PRD_PRG_FLAG_13     CHAR(1) NOT NULL WITH DEFAULT,
           PRD_PRG_FLAG_14     CHAR(1) NOT NULL WITH DEFAULT)
           END-EXEC.
       01  FLY-FLY-FSCL-YEAR.
           05  FLY-USER-CD                    PIC X(08).
           05  FLY-LAST-ACTVY-DT              PIC X(05).
           05  FLY-UNVRS-CD                   PIC X(02).
           05  FLY-COA-CD                     PIC X(01).
           05  FLY-FSCL-YR                    PIC X(02).
           05  FLY-ACTVY-DT                   PIC X(10).
           05  FLY-NBR-OF-PRDS                PIC S9(02)         COMP-3.
           05  FLY-FSCL-YR-START-DT           PIC X(10).
           05  FLY-FSCL-YR-END-DT             PIC X(10).
           05  FLY-PRD-STATUS-01              PIC X(01).
           05  FLY-PRD-STATUS-02              PIC X(01).
           05  FLY-PRD-STATUS-03              PIC X(01).
           05  FLY-PRD-STATUS-04              PIC X(01).
           05  FLY-PRD-STATUS-05              PIC X(01).
           05  FLY-PRD-STATUS-06              PIC X(01).
           05  FLY-PRD-STATUS-07              PIC X(01).
           05  FLY-PRD-STATUS-08              PIC X(01).
           05  FLY-PRD-STATUS-09              PIC X(01).
           05  FLY-PRD-STATUS-10              PIC X(01).
           05  FLY-PRD-STATUS-11              PIC X(01).
           05  FLY-PRD-STATUS-12              PIC X(01).
           05  FLY-PRD-STATUS-13              PIC X(01).
           05  FLY-PRD-START-DT-01            PIC X(10).
           05  FLY-PRD-START-DT-02            PIC X(10).
           05  FLY-PRD-START-DT-03            PIC X(10).
           05  FLY-PRD-START-DT-04            PIC X(10).
           05  FLY-PRD-START-DT-05            PIC X(10).
           05  FLY-PRD-START-DT-06            PIC X(10).
           05  FLY-PRD-START-DT-07            PIC X(10).
           05  FLY-PRD-START-DT-08            PIC X(10).
           05  FLY-PRD-START-DT-09            PIC X(10).
           05  FLY-PRD-START-DT-10            PIC X(10).
           05  FLY-PRD-START-DT-11            PIC X(10).
           05  FLY-PRD-START-DT-12            PIC X(10).
           05  FLY-PRD-START-DT-13            PIC X(10).
           05  FLY-PRD-END-DT-01              PIC X(10).
           05  FLY-PRD-END-DT-02              PIC X(10).
           05  FLY-PRD-END-DT-03              PIC X(10).
           05  FLY-PRD-END-DT-04              PIC X(10).
           05  FLY-PRD-END-DT-05              PIC X(10).
           05  FLY-PRD-END-DT-06              PIC X(10).
           05  FLY-PRD-END-DT-07              PIC X(10).
           05  FLY-PRD-END-DT-08              PIC X(10).
           05  FLY-PRD-END-DT-09              PIC X(10).
           05  FLY-PRD-END-DT-10              PIC X(10).
           05  FLY-PRD-END-DT-11              PIC X(10).
           05  FLY-PRD-END-DT-12              PIC X(10).
           05  FLY-PRD-END-DT-13              PIC X(10).
           05  FLY-END-OF-QTR-IND-01          PIC X(01).
           05  FLY-END-OF-QTR-IND-02          PIC X(01).
           05  FLY-END-OF-QTR-IND-03          PIC X(01).
           05  FLY-END-OF-QTR-IND-04          PIC X(01).
           05  FLY-END-OF-QTR-IND-05          PIC X(01).
           05  FLY-END-OF-QTR-IND-06          PIC X(01).
           05  FLY-END-OF-QTR-IND-07          PIC X(01).
           05  FLY-END-OF-QTR-IND-08          PIC X(01).
           05  FLY-END-OF-QTR-IND-09          PIC X(01).
           05  FLY-END-OF-QTR-IND-10          PIC X(01).
           05  FLY-END-OF-QTR-IND-11          PIC X(01).
           05  FLY-END-OF-QTR-IND-12          PIC X(01).
           05  FLY-END-OF-QTR-IND-13          PIC X(01).
           05  FLY-ACRL-PRD-STATUS            PIC X(01).
           05  FLY-BEG-PURGE-FLAG             PIC X(01).
           05  FLY-PRD-PRG-FLAG-01            PIC X(01).
           05  FLY-PRD-PRG-FLAG-02            PIC X(01).
           05  FLY-PRD-PRG-FLAG-03            PIC X(01).
           05  FLY-PRD-PRG-FLAG-04            PIC X(01).
           05  FLY-PRD-PRG-FLAG-05            PIC X(01).
           05  FLY-PRD-PRG-FLAG-06            PIC X(01).
           05  FLY-PRD-PRG-FLAG-07            PIC X(01).
           05  FLY-PRD-PRG-FLAG-08            PIC X(01).
           05  FLY-PRD-PRG-FLAG-09            PIC X(01).
           05  FLY-PRD-PRG-FLAG-10            PIC X(01).
           05  FLY-PRD-PRG-FLAG-11            PIC X(01).
           05  FLY-PRD-PRG-FLAG-12            PIC X(01).
           05  FLY-PRD-PRG-FLAG-13            PIC X(01).
           05  FLY-PRD-PRG-FLAG-14            PIC X(01).
