           EXEC SQL DECLARE IVH_INV_HDR_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           UNVRS_CD            CHAR(2) NOT NULL WITH DEFAULT,
           DOC_NBR             CHAR(8) NOT NULL,
           PYMT_DUE_DT         DATE NOT NULL WITH DEFAULT,
           BANK_ACCT_CD        CHAR(2) NOT NULL WITH DEFAULT,
           SALES_USE_TAX_IND   CHAR(1) NOT NULL WITH DEFAULT,
           A1099_IND           CHAR(1) NOT NULL WITH DEFAULT,
           CRDT_MEMO           CHAR(1) NOT NULL WITH DEFAULT,
           CHECK_GRPNG_IND     CHAR(1) NOT NULL WITH DEFAULT,
           CMPLT_IND           CHAR(1) NOT NULL WITH DEFAULT,
           RSBLE_IND           CHAR(1) NOT NULL WITH DEFAULT,
           ADDL_CHRG           DECIMAL(12,2) NOT NULL WITH DEFAULT,
           ADJMT_CD            CHAR(2) NOT NULL WITH DEFAULT,
           ADJMT_DT            DATE NOT NULL WITH DEFAULT,
           OPN_PD_HLD_IND      CHAR(1) NOT NULL WITH DEFAULT,
           INVD_AMT            DECIMAL(12,2) NOT NULL WITH DEFAULT,
           APRVL_IND           CHAR(1) NOT NULL WITH DEFAULT,
           HDR_ERROR_IND       CHAR(1) NOT NULL WITH DEFAULT,
           HDR_CNTR_DTL        DECIMAL(4) NOT NULL WITH DEFAULT,
           HDR_CNTR_ACCT       DECIMAL(4) NOT NULL WITH DEFAULT,
           ADR_TYP_CD          CHAR(2) NOT NULL WITH DEFAULT,
           PO_NBR              CHAR(8) NOT NULL WITH DEFAULT,
           DSCNT_CD            CHAR(2) NOT NULL WITH DEFAULT,
           VNDR_INV_NBR        CHAR(9) NOT NULL WITH DEFAULT,
           TAX_RATE_CD         CHAR(3) NOT NULL WITH DEFAULT,
           VNDR_ID_DGT_ONE     CHAR(1) NOT NULL WITH DEFAULT,
           VNDR_ID_LST_NINE    CHAR(9) NOT NULL WITH DEFAULT,
           INCM_TYP_SEQ_NBR    DECIMAL(4) NOT NULL WITH DEFAULT,
           A1099_RPRT_ID       CHAR(9) NOT NULL WITH DEFAULT,
           TRANS_DT            DATE NOT NULL WITH DEFAULT,
           CNCL_IND            CHAR(1) NOT NULL WITH DEFAULT,
           CNCL_DT             DATE NOT NULL WITH DEFAULT,
           CHECKS_WRITTEN_QTY  DECIMAL(5) NOT NULL WITH DEFAULT,
           VNDR_INV_DT         DATE NOT NULL WITH DEFAULT,
           DOC_REF_NBR         CHAR(10) NOT NULL WITH DEFAULT,
           MAILCODE            CHAR(6) NOT NULL WITH DEFAULT,
           SUB_DOC_TYP         CHAR(3) NOT NULL WITH DEFAULT,
           ORGN_CD             CHAR(4) NOT NULL WITH DEFAULT,
           IDC_APPL_AMT        DECIMAL(7,2) NOT NULL WITH DEFAULT,
           IDC_ACTL_AMT        DECIMAL(7,2) NOT NULL WITH DEFAULT,
           VDR_I_SETF1         CHAR(1) NOT NULL WITH DEFAULT,
           FK1_VDR_IREF_ID     DECIMAL(7) NOT NULL WITH DEFAULT,
           FK1_VDR_ENTPSN_IND  CHAR(1) NOT NULL WITH DEFAULT,
           VDR_V_SETF2         CHAR(1) NOT NULL WITH DEFAULT,
           FK2_VDR_IREF_ID     DECIMAL(7) NOT NULL WITH DEFAULT,
           FK2_VDR_ENTPSN_IND  CHAR(1) NOT NULL WITH DEFAULT,
           VDR_SET_TS          TIMESTAMP NOT NULL WITH DEFAULT,
           IDX_INV_SETF        CHAR(1) NOT NULL WITH DEFAULT,
           RSV_SETF            CHAR(1) NOT NULL WITH DEFAULT,
           FK_RSV_RSBLE_ID     CHAR(3) NOT NULL WITH DEFAULT,
           RSV_SET_TS          TIMESTAMP NOT NULL WITH DEFAULT,
           ACRL_FSCL_YR        CHAR(2) NOT NULL,
           FTB_592_IND         CHAR(1) NOT NULL WITH DEFAULT,
           CA_WH_AMT           DECIMAL(8,2) NOT NULL WITH DEFAULT,
           CA_RPT_AMT          DECIMAL(8,2) NOT NULL WITH DEFAULT,
FP2006     FORCE_CHECK         CHAR(1) NOT NULL WITH DEFAULT,
FP7157     TAX_TREATY_IND      CHAR(1) NOT NULL,
FP7157     IRS_INCOME_CD       CHAR(2) NOT NULL,
S41335     FED_WH_AMT          DECIMAL(8,2) NOT NULL WITH DEFAULT,
S41335     FED_RPT_AMT         DECIMAL(8,2) NOT NULL WITH DEFAULT
         ) END-EXEC.
       01  IVH-IVH-INV-HDR.
           05  IVH-USER-CD                    PIC X(08).
           05  IVH-LAST-ACTVY-DT              PIC X(05).
           05  IVH-UNVRS-CD                   PIC X(02).
           05  IVH-DOC-NBR                    PIC X(08).
           05  IVH-PYMT-DUE-DT                PIC X(10).
           05  IVH-BANK-ACCT-CD               PIC X(02).
           05  IVH-SALES-USE-TAX-IND          PIC X(01).
           05  IVH-A1099-IND                  PIC X(01).
           05  IVH-CRDT-MEMO                  PIC X(01).
           05  IVH-CHECK-GRPNG-IND            PIC X(01).
           05  IVH-CMPLT-IND                  PIC X(01).
           05  IVH-RSBLE-IND                  PIC X(01).
           05  IVH-ADDL-CHRG                  PIC S9(10)V99      COMP-3.
           05  IVH-ADJMT-CD                   PIC X(02).
           05  IVH-ADJMT-DT                   PIC X(10).
           05  IVH-OPN-PD-HLD-IND             PIC X(01).
           05  IVH-INVD-AMT                   PIC S9(10)V99      COMP-3.
           05  IVH-APRVL-IND                  PIC X(01).
           05  IVH-HDR-ERROR-IND              PIC X(01).
           05  IVH-HDR-CNTR-DTL               PIC S9(04)         COMP-3.
           05  IVH-HDR-CNTR-ACCT              PIC S9(04)         COMP-3.
           05  IVH-ADR-TYP-CD                 PIC X(02).
           05  IVH-PO-NBR                     PIC X(08).
           05  IVH-DSCNT-CD                   PIC X(02).
           05  IVH-VNDR-INV-NBR               PIC X(09).
           05  IVH-TAX-RATE-CD                PIC X(03).
           05  IVH-VNDR-ID-DGT-ONE            PIC X(01).
           05  IVH-VNDR-ID-LST-NINE           PIC X(09).
           05  IVH-INCM-TYP-SEQ-NBR           PIC S9(04)         COMP-3.
           05  IVH-A1099-RPRT-ID              PIC X(09).
           05  IVH-TRANS-DT                   PIC X(10).
           05  IVH-CNCL-IND                   PIC X(01).
           05  IVH-CNCL-DT                    PIC X(10).
           05  IVH-CHECKS-WRITTEN-QTY         PIC S9(05)         COMP-3.
           05  IVH-VNDR-INV-DT                PIC X(10).
           05  IVH-DOC-REF-NBR                PIC X(10).
           05  IVH-MAILCODE                   PIC X(06).
           05  IVH-SUB-DOC-TYP                PIC X(03).
           05  IVH-ORGN-CD                    PIC X(04).
           05  IVH-IDC-APPL-AMT               PIC S9(05)V99      COMP-3.
           05  IVH-IDC-ACTL-AMT               PIC S9(05)V99      COMP-3.
           05  IVH-VDR-I-SETF1                PIC X(1).
           05  IVH-FK1-VDR-IREF-ID            PIC S9(07)         COMP-3.
           05  IVH-FK1-VDR-ENTPSN-IND         PIC X(01).
           05  IVH-VDR-V-SETF2                PIC X(1).
           05  IVH-FK2-VDR-IREF-ID            PIC S9(07)         COMP-3.
           05  IVH-FK2-VDR-ENTPSN-IND         PIC X(01).
           05  IVH-VDR-SET-TS                 PIC X(26).
           05  IVH-IDX-INV-SETF               PIC X(1).
           05  IVH-RSV-SETF                   PIC X(1).
           05  IVH-FK-RSV-RSBLE-ID            PIC X(03).
           05  IVH-RSV-SET-TS                 PIC X(26).
           05  IVH-ACRL-FSCL-YR               PIC X(2).
           05  IVH-FTB-592-IND                PIC X(1).
           05  IVH-CA-WH-AMT                  PIC S9(8)V99       COMP-3.
           05  IVH-CA-RPT-AMT                 PIC S9(8)V99       COMP-3.
FP2006     05  IVH-FORCE-CHECK                PIC X(1).
FP7157     05  IVH-TAX-TREATY-IND             PIC X(1).
FP7157     05  IVH-IRS-INCOME-CD              PIC X(2).
S41335     05  IVH-FED-WH-AMT                 PIC S9(8)V99       COMP-3.
S41335     05  IVH-FED-RPT-AMT                PIC S9(8)V99       COMP-3.
