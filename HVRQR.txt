
           EXEC SQL DECLARE RQR_RQST_RPT_J_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           END_DT              DATE NOT NULL WITH DEFAULT,
           NBR_OF_COPIES       DECIMAL(3) NOT NULL WITH DEFAULT,
           RPRT_CD             CHAR(8) NOT NULL,
           FK_RID_RPRT_RQSTID  CHAR(4) NOT NULL,
           FK_RPT_RPRT_CD      CHAR(8) NOT NULL WITH DEFAULT,
           RPT_SET_TS          TIMESTAMP NOT NULL WITH DEFAULT)
           END-EXEC.
       01  RQR-RQR-RQST-RPT-J.
           05  RQR-USER-CD                    PIC X(08).
           05  RQR-LAST-ACTVY-DT              PIC X(05).
           05  RQR-END-DT                     PIC X(10).
           05  RQR-NBR-OF-COPIES              PIC S9(03)         COMP-3.
           05  RQR-RPRT-CD                    PIC X(08).
           05  RQR-FK-RID-RPRT-RQSTID         PIC X(04).
           05  RQR-FK-RPT-RPRT-CD             PIC X(08).
           05  RQR-RPT-SET-TS                 PIC X(26).
