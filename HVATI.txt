
           EXEC SQL DECLARE ATI_ACTI_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           UNVRS_CD            CHAR(2) NOT NULL WITH DEFAULT,
           COA_CD              CHAR(1) NOT NULL WITH DEFAULT,
           ACCT_INDX_CD        CHAR(10) NOT NULL,
           ACTVY_DT            DATE NOT NULL WITH DEFAULT)
           END-EXEC.
       01  ATI-ATI-ACTI.
           05  ATI-USER-CD                    PIC X(08).
           05  ATI-LAST-ACTVY-DT              PIC X(05).
           05  ATI-UNVRS-CD                   PIC X(02).
           05  ATI-COA-CD                     PIC X(01).
           05  ATI-ACCT-INDX-CD               PIC X(10).
           05  ATI-ACTVY-DT                   PIC X(10).
