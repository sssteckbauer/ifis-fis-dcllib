
           EXEC SQL DECLARE INR_INDRT_RCPT_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           DSTBN_PCT           DECIMAL(6,3) NOT NULL WITH DEFAULT,
           ACCT_INDX_CD        CHAR(10) NOT NULL WITH DEFAULT,
           FUND_CD             CHAR(6) NOT NULL WITH DEFAULT,
           ORGN_CD             CHAR(6) NOT NULL WITH DEFAULT,
           ACCT_CD             CHAR(6) NOT NULL WITH DEFAULT,
           PRGRM_CD            CHAR(6) NOT NULL WITH DEFAULT,
           ACTVY_CD            CHAR(6) NOT NULL WITH DEFAULT,
           LCTN_CD             CHAR(6) NOT NULL WITH DEFAULT,
           INDRT_COST_ACCT     CHAR(6) NOT NULL WITH DEFAULT,
           FK_ICE_IND_COST_CD  CHAR(6) NOT NULL,
           FK_ICE_START_DT     DATE NOT NULL,
           FK_ICE_TIME_STMP    TIME NOT NULL,
           ICE_SET_TS          TIMESTAMP NOT NULL)
           END-EXEC.
       01  INR-INR-INDRT-RCPT.
           05  INR-USER-CD                    PIC X(08).
           05  INR-LAST-ACTVY-DT              PIC X(05).
           05  INR-DSTBN-PCT                  PIC S9(03)V999     COMP-3.
           05  INR-ACCT-INDX-CD               PIC X(10).
           05  INR-FUND-CD                    PIC X(06).
           05  INR-ORGN-CD                    PIC X(06).
           05  INR-ACCT-CD                    PIC X(06).
           05  INR-PRGRM-CD                   PIC X(06).
           05  INR-ACTVY-CD                   PIC X(06).
           05  INR-LCTN-CD                    PIC X(06).
           05  INR-INDRT-COST-ACCT            PIC X(06).
           05  INR-FK-ICE-IND-COST-CD         PIC X(06).
           05  INR-FK-ICE-START-DT            PIC X(10).
           05  INR-FK-ICE-TIME-STMP           PIC X(8).
           05  INR-ICE-SET-TS                 PIC X(26).
