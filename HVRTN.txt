
           EXEC SQL DECLARE RTN_RETURNS_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           UNVRS_CD            CHAR(2) NOT NULL WITH DEFAULT,
           PO_RTRN_CD          CHAR(8) NOT NULL,
           CARR_IREF_ID        DECIMAL(7) NOT NULL WITH DEFAULT,
           ADR_TYP_CD          CHAR(2) NOT NULL WITH DEFAULT,
           PYMT_CD             CHAR(2) NOT NULL WITH DEFAULT,
           NBR_PCS             DECIMAL(4) NOT NULL WITH DEFAULT,
           FRGHT_CHRG          DECIMAL(12,2) NOT NULL WITH DEFAULT,
           SHIP_WT             CHAR(8) NOT NULL WITH DEFAULT,
           BILL_OF_LADING      CHAR(10) NOT NULL WITH DEFAULT,
           FRGN_DMSTC_IND      CHAR(1) NOT NULL WITH DEFAULT,
           VNDR_IREF_ID        DECIMAL(7) NOT NULL WITH DEFAULT,
           CONSGNE_NAME        CHAR(35) NOT NULL WITH DEFAULT,
           CARR_ENTPSN_IND     CHAR(1) NOT NULL WITH DEFAULT,
           ENTPSN_IND          CHAR(1) NOT NULL WITH DEFAULT,
           DOC_REF_NBR         CHAR(10) NOT NULL WITH DEFAULT)
           END-EXEC.
       01  RTN-RTN-RETURNS.
           05  RTN-USER-CD                    PIC X(08).
           05  RTN-LAST-ACTVY-DT              PIC X(05).
           05  RTN-UNVRS-CD                   PIC X(02).
           05  RTN-PO-RTRN-CD                 PIC X(08).
           05  RTN-CARR-IREF-ID               PIC S9(07)         COMP-3.
           05  RTN-ADR-TYP-CD                 PIC X(02).
           05  RTN-PYMT-CD                    PIC X(02).
           05  RTN-NBR-PCS                    PIC S9(04)         COMP-3.
           05  RTN-FRGHT-CHRG                 PIC S9(10)V99      COMP-3.
           05  RTN-SHIP-WT                    PIC X(08).
           05  RTN-BILL-OF-LADING             PIC X(10).
           05  RTN-FRGN-DMSTC-IND             PIC X(01).
           05  RTN-VNDR-IREF-ID               PIC S9(07)         COMP-3.
           05  RTN-CONSGNE-NAME               PIC X(35).
           05  RTN-CARR-ENTPSN-IND            PIC X(01).
           05  RTN-ENTPSN-IND                 PIC X(01).
           05  RTN-DOC-REF-NBR                PIC X(10).
