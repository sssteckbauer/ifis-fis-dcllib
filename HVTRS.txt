
           EXEC SQL DECLARE TRS_TRNST_RISK_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           UNVRS_CD            CHAR(2) NOT NULL WITH DEFAULT,
           TRNST_RISK_CD       CHAR(2) NOT NULL,
           TRNST_RISK_DESC     CHAR(35) NOT NULL WITH DEFAULT,
           START_DT            DATE NOT NULL WITH DEFAULT,
           END_DT              DATE NOT NULL WITH DEFAULT,
           ACTVY_DT            DATE NOT NULL WITH DEFAULT)
           END-EXEC.
       01  TRS-TRS-TRNST-RISK.
           05  TRS-USER-CD                    PIC X(08).
           05  TRS-LAST-ACTVY-DT              PIC X(05).
           05  TRS-UNVRS-CD                   PIC X(02).
           05  TRS-TRNST-RISK-CD              PIC X(02).
           05  TRS-TRNST-RISK-DESC            PIC X(35).
           05  TRS-START-DT                   PIC X(10).
           05  TRS-END-DT                     PIC X(10).
           05  TRS-ACTVY-DT                   PIC X(10).
