
FCSAH *    EXEC SQL DECLARE MAP_MAP TABLE
FCSAH      EXEC SQL DECLARE MAP_MAP_V TABLE
           (MAP_NAME           CHAR(8) NOT NULL,
           DESCR               CHAR(40) NOT NULL WITH DEFAULT,
           PKEY_TS             TIMESTAMP NOT NULL)
           END-EXEC.
       01  MAP-MAP-MAP.
           05  MAP-MAP-NAME                   PIC X(08).
           05  MAP-DESCR                      PIC X(40).
           05  MAP-PKEY-TS                    PIC X(26).
