      ******************************************************************
      * DCLGEN TABLE(ASD_ASSET_DESC_V                                  *
      *        LANGUAGE(COBOL)                                         *
      *        NAMES(ASD-)                                             *
      *        STRUCTURE(ASD-ASD-ASSET-DESC)                           *
      *        QUOTE                                                   *
      *        COLSUFFIX(YES)                                          *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE ASD_ASSET_DESC_V TABLE
           ( USER_CD                        CHAR(8) NOT NULL,
             LAST_ACTVY_DT                  CHAR(5) NOT NULL,
             FK_PO_NBR                      CHAR(8) NOT NULL,
             FK_PO_SEQ_NBR                  CHAR(3) NOT NULL,
             FK_PO_ITEM_NBR                 DECIMAL(4, 0) NOT NULL,
             EAMS_DESC                      CHAR(50) NOT NULL,
             TRADE_IN_IND                   CHAR(1) NOT NULL,
             TRADE_IN_UCID_NBR              CHAR(9) NOT NULL,
             ACSRY_IND                      CHAR(1) NOT NULL,
             ACSRY_UCID_NBR                 CHAR(9) NOT NULL,
             FAB_IND                        CHAR(1) NOT NULL,
             FAB_NBR                        CHAR(4) NOT NULL,
             END_DATE                       DATE    NOT NULL,
             LAST_ACTVY_DT_STMP             TIMESTAMP NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE ASD_ASSET_DESC_V                   *
      ******************************************************************
       01  ASD-ASD-ASSET-DESC.
      *                       USER_CD
           10 ASD-USER-CD          PIC X(8).
      *                       LAST_ACTVY_DT
           10 ASD-LAST-ACTVY-DT    PIC X(5).
      *                       FK_PO_NBR
           10 ASD-FK-PO-NBR        PIC X(8).
      *                       FK_PO_SEQ_NBR
           10 ASD-FK-PO-SEQ-NBR    PIC X(3).
      *                       FK_PO_ITEM_NBR
           10 ASD-FK-PO-ITEM-NBR   PIC S9(4)V USAGE COMP-3.
      *                       EAMS_DESC
           10 ASD-EAMS-DESC        PIC X(50).
      *                       TRADE_IN_IND
           10 ASD-TRADE-IN-IND     PIC X(1).
      *                       TRADE_IN_UCID_NBR
           10 ASD-TRADE-IN-UCID-NBR  PIC X(9).
      *                       ACSRY_IND
           10 ASD-ACSRY-IND        PIC X(1).
      *                       ACSRY_UCID_NBR
           10 ASD-ACSRY-UCID-NBR   PIC X(9).
      *                       FAB_IND
           10 ASD-FAB-IND          PIC X(1).
      *                       FAB_NBR
           10 ASD-FAB-NBR          PIC X(4).
      *                       END_DATE
           10 ASD-END-DATE         PIC X(10).
      *                       LAST_ACTVY_DT_STMP
           10 ASD-LAST-ACTVY-DT-STMP  PIC X(26).
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 14      *
      ******************************************************************
