
FCSAH *    EXEC SQL DECLARE INQ_INQ TABLE
FCSAH      EXEC SQL DECLARE INQ_INQ_V TABLE
           (INQ_NAM            CHAR(32) NOT NULL,
           ELEM_VER            DECIMAL(4) NOT NULL WITH DEFAULT,
           DATE_LU             CHAR(8) NOT NULL WITH DEFAULT,
           BUILDER             CHAR(1) NOT NULL WITH DEFAULT,
           DATE_CREATED        CHAR(8) NOT NULL WITH DEFAULT,
           PREP_BY             CHAR(8) NOT NULL WITH DEFAULT,
           REV_BY              CHAR(8) NOT NULL WITH DEFAULT,
           DESC                CHAR(64) NOT NULL WITH DEFAULT,
           VAL_SW              CHAR(1) NOT NULL WITH DEFAULT,
           ALT_PIC_TYPE_1      DECIMAL(4) NOT NULL WITH DEFAULT,
           ELEM_LGTH_1         DECIMAL(4) NOT NULL WITH DEFAULT,
           PIC_LGTH_1          DECIMAL(4) NOT NULL WITH DEFAULT,
           USE_1               DECIMAL(4) NOT NULL WITH DEFAULT,
           ELEM_JUST_1         CHAR(1) NOT NULL WITH DEFAULT,
           ELEM_BONZ_1         CHAR(1) NOT NULL WITH DEFAULT,
           SIGN_1              CHAR(1) NOT NULL WITH DEFAULT,
           SEPARATE_1          CHAR(1) NOT NULL WITH DEFAULT,
           PIC_1               CHAR(30) NOT NULL WITH DEFAULT,
           PIC_FLAG_1          CHAR(8) NOT NULL WITH DEFAULT,
           ALT_PIC_TYPE_2      DECIMAL(4) NOT NULL WITH DEFAULT,
           ELEM_LGTH_2         DECIMAL(4) NOT NULL WITH DEFAULT,
           PIC_LGTH_2          DECIMAL(4) NOT NULL WITH DEFAULT,
           USE_2               DECIMAL(4) NOT NULL WITH DEFAULT,
           ELEM_JUST_2         CHAR(1) NOT NULL WITH DEFAULT,
           ELEM_BONZ_2         CHAR(1) NOT NULL WITH DEFAULT,
           SIGN_2              CHAR(1) NOT NULL WITH DEFAULT,
           SEPARATE_2          CHAR(1) NOT NULL WITH DEFAULT,
           PIC_2               CHAR(30) NOT NULL WITH DEFAULT,
           PIC_FLAG_2          CHAR(8) NOT NULL WITH DEFAULT,
           ALT_PIC_TYPE_3      DECIMAL(4) NOT NULL WITH DEFAULT,
           ELEM_LGTH_3         DECIMAL(4) NOT NULL WITH DEFAULT,
           PIC_LGTH_3          DECIMAL(4) NOT NULL WITH DEFAULT,
           USE_3               DECIMAL(4) NOT NULL WITH DEFAULT,
           ELEM_JUST_3         CHAR(1) NOT NULL WITH DEFAULT,
           ELEM_BONZ_3         CHAR(1) NOT NULL WITH DEFAULT,
           SIGN_3              CHAR(1) NOT NULL WITH DEFAULT,
           SEPARATE_3          CHAR(1) NOT NULL WITH DEFAULT,
           PIC_3               CHAR(30) NOT NULL WITH DEFAULT,
           PIC_FLAG_3          CHAR(8) NOT NULL WITH DEFAULT,
           ALT_PIC_TYPE_4      DECIMAL(4) NOT NULL WITH DEFAULT,
           ELEM_LGTH_4         DECIMAL(4) NOT NULL WITH DEFAULT,
           PIC_LGTH_4          DECIMAL(4) NOT NULL WITH DEFAULT,
           USE_4               DECIMAL(4) NOT NULL WITH DEFAULT,
           ELEM_JUST_4         CHAR(1) NOT NULL WITH DEFAULT,
           ELEM_BONZ_4         CHAR(1) NOT NULL WITH DEFAULT,
           SIGN_4              CHAR(1) NOT NULL WITH DEFAULT,
           SEPARATE_4          CHAR(1) NOT NULL WITH DEFAULT,
           PIC_4               CHAR(30) NOT NULL WITH DEFAULT,
           PIC_FLAG_4          CHAR(8) NOT NULL WITH DEFAULT,
           ALT_PIC_TYPE_5      DECIMAL(4) NOT NULL WITH DEFAULT,
           ELEM_LGTH_5         DECIMAL(4) NOT NULL WITH DEFAULT,
           PIC_LGTH_5          DECIMAL(4) NOT NULL WITH DEFAULT,
           USE_5               DECIMAL(4) NOT NULL WITH DEFAULT,
           ELEM_JUST_5         CHAR(1) NOT NULL WITH DEFAULT,
           ELEM_BONZ_5         CHAR(1) NOT NULL WITH DEFAULT,
           SIGN_5              CHAR(1) NOT NULL WITH DEFAULT,
           SEPARATE_5          CHAR(1) NOT NULL WITH DEFAULT,
           PIC_5               CHAR(30) NOT NULL WITH DEFAULT,
           PIC_FLAG_5          CHAR(8) NOT NULL WITH DEFAULT,
           PUB_ACCESS_FLG      CHAR(8) NOT NULL WITH DEFAULT,
           USER_COUNT          DECIMAL(4) NOT NULL WITH DEFAULT,
           PKEY_TS             TIMESTAMP NOT NULL)
           END-EXEC.
       01  INQ-INQ-INQ.
           05  INQ-INQ-NAM                    PIC X(32).
           05  INQ-ELEM-VER                   PIC S9(4)          COMP-3.
           05  INQ-DATE-LU                    PIC X(08).
           05  INQ-BUILDER                    PIC X(01).
           05  INQ-DATE-CREATED               PIC X(08).
           05  INQ-PREP-BY                    PIC X(08).
           05  INQ-REV-BY                     PIC X(08).
           05  INQ-DESC                       PIC X(64).
           05  INQ-VAL-SW                     PIC X(01).
           05  INQ-ALT-PIC-TYPE-1             PIC S9(4)          COMP-3.
           05  INQ-ALT-PIC-TYPE-2             PIC S9(4)          COMP-3.
           05  INQ-ALT-PIC-TYPE-3             PIC S9(4)          COMP-3.
           05  INQ-ALT-PIC-TYPE-4             PIC S9(4)          COMP-3.
           05  INQ-ALT-PIC-TYPE-5             PIC S9(4)          COMP-3.
           05  INQ-ELEM-LGTH-1                PIC S9(4)          COMP-3.
           05  INQ-ELEM-LGTH-2                PIC S9(4)          COMP-3.
           05  INQ-ELEM-LGTH-3                PIC S9(4)          COMP-3.
           05  INQ-ELEM-LGTH-4                PIC S9(4)          COMP-3.
           05  INQ-ELEM-LGTH-5                PIC S9(4)          COMP-3.
           05  INQ-PIC-LGTH-1                 PIC S9(4)          COMP-3.
           05  INQ-PIC-LGTH-2                 PIC S9(4)          COMP-3.
           05  INQ-PIC-LGTH-3                 PIC S9(4)          COMP-3.
           05  INQ-PIC-LGTH-4                 PIC S9(4)          COMP-3.
           05  INQ-PIC-LGTH-5                 PIC S9(4)          COMP-3.
           05  INQ-USE-1                      PIC S9(4)          COMP-3.
           05  INQ-USE-2                      PIC S9(4)          COMP-3.
           05  INQ-USE-3                      PIC S9(4)          COMP-3.
           05  INQ-USE-4                      PIC S9(4)          COMP-3.
           05  INQ-USE-5                      PIC S9(4)          COMP-3.
           05  INQ-ELEM-JUST-1                PIC X(01).
           05  INQ-ELEM-JUST-2                PIC X(01).
           05  INQ-ELEM-JUST-3                PIC X(01).
           05  INQ-ELEM-JUST-4                PIC X(01).
           05  INQ-ELEM-JUST-5                PIC X(01).
           05  INQ-ELEM-BONZ-1                PIC X(01).
           05  INQ-ELEM-BONZ-2                PIC X(01).
           05  INQ-ELEM-BONZ-3                PIC X(01).
           05  INQ-ELEM-BONZ-4                PIC X(01).
           05  INQ-ELEM-BONZ-5                PIC X(01).
           05  INQ-SIGN-1                     PIC X(01).
           05  INQ-SIGN-2                     PIC X(01).
           05  INQ-SIGN-3                     PIC X(01).
           05  INQ-SIGN-4                     PIC X(01).
           05  INQ-SIGN-5                     PIC X(01).
           05  INQ-SEPARATE-1                 PIC X(01).
           05  INQ-SEPARATE-2                 PIC X(01).
           05  INQ-SEPARATE-3                 PIC X(01).
           05  INQ-SEPARATE-4                 PIC X(01).
           05  INQ-SEPARATE-5                 PIC X(01).
           05  INQ-PIC-1                      PIC X(30).
           05  INQ-PIC-2                      PIC X(30).
           05  INQ-PIC-3                      PIC X(30).
           05  INQ-PIC-4                      PIC X(30).
           05  INQ-PIC-5                      PIC X(30).
           05  INQ-PIC-FLAG-1                 PIC X(08).
           05  INQ-PIC-FLAG-2                 PIC X(08).
           05  INQ-PIC-FLAG-3                 PIC X(08).
           05  INQ-PIC-FLAG-4                 PIC X(08).
           05  INQ-PIC-FLAG-5                 PIC X(08).
           05  INQ-PUB-ACCESS-FLG             PIC X(08).
           05  INQ-USER-COUNT                 PIC S9(4)          COMP-3.
           05  INQ-PKEY-TS                    PIC X(26).
