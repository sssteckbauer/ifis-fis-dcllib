
           EXEC SQL DECLARE TRE_TRIP_ENC_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           SEQ_NBR             DECIMAL(4) NOT NULL,
           COA_CD              CHAR(1) NOT NULL WITH DEFAULT,
           ACCT_INDX_CD        CHAR(10) NOT NULL WITH DEFAULT,
           FUND_CD             CHAR(6) NOT NULL WITH DEFAULT,
           ORGN_CD             CHAR(6) NOT NULL WITH DEFAULT,
           ACCT_CD             CHAR(6) NOT NULL WITH DEFAULT,
           PRGRM_CD            CHAR(6) NOT NULL WITH DEFAULT,
           ACTVY_CD            CHAR(6) NOT NULL WITH DEFAULT,
           LCTN_CD             CHAR(6) NOT NULL WITH DEFAULT,
           ACCT_ERROR_IND      CHAR(1) NOT NULL WITH DEFAULT,
           ENCMBR_AMT          DECIMAL(12,2) NOT NULL WITH DEFAULT,
           RULE_CLS_CD         CHAR(4) NOT NULL WITH DEFAULT,
           NSF_OVRDE           CHAR(1) NOT NULL WITH DEFAULT,
           FSCL_YR             CHAR(2) NOT NULL WITH DEFAULT,
           PSTNG_PRD           CHAR(2) NOT NULL WITH DEFAULT,
           LIEN_CLOSED_CD      CHAR(1) NOT NULL WITH DEFAULT,
           FK_TRP_TRIP_NBR     CHAR(8) NOT NULL)
           END-EXEC.
       01  TRE-TRE-TRIP-ENC.
           05  TRE-USER-CD                    PIC X(08).
           05  TRE-LAST-ACTVY-DT              PIC X(05).
           05  TRE-SEQ-NBR                    PIC S9(04)         COMP-3.
           05  TRE-COA-CD                     PIC X(01).
           05  TRE-ACCT-INDX-CD               PIC X(10).
           05  TRE-FUND-CD                    PIC X(06).
           05  TRE-ORGN-CD                    PIC X(06).
           05  TRE-ACCT-CD                    PIC X(06).
           05  TRE-PRGRM-CD                   PIC X(06).
           05  TRE-ACTVY-CD                   PIC X(06).
           05  TRE-LCTN-CD                    PIC X(06).
           05  TRE-ACCT-ERROR-IND             PIC X(01).
           05  TRE-ENCMBR-AMT                 PIC S9(10)V99      COMP-3.
           05  TRE-RULE-CLS-CD                PIC X(04).
           05  TRE-NSF-OVRDE                  PIC X(01).
           05  TRE-FSCL-YR                    PIC X(02).
           05  TRE-PSTNG-PRD                  PIC X(02).
           05  TRE-LIEN-CLOSED-CD             PIC X(01).
           05  TRE-FK-TRP-TRIP-NBR            PIC X(08).
