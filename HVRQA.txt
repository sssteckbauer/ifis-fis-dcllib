
           EXEC SQL DECLARE RQA_RQST_ACCT_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           SEQ_NBR             DECIMAL(4) NOT NULL,
           PCT_DSTBN           DECIMAL(6,3) NOT NULL WITH DEFAULT,
           ACTVY_DT            DATE NOT NULL WITH DEFAULT,
           ACCT_AMT            DECIMAL(12,2) NOT NULL WITH DEFAULT,
           ACCT_ERROR_IND      CHAR(1) NOT NULL WITH DEFAULT,
           COA_CD              CHAR(1) NOT NULL WITH DEFAULT,
           ACCT_INDX_CD        CHAR(10) NOT NULL WITH DEFAULT,
           FUND_CD             CHAR(6) NOT NULL WITH DEFAULT,
           ORGN_CD             CHAR(6) NOT NULL WITH DEFAULT,
           ACCT_CD             CHAR(6) NOT NULL WITH DEFAULT,
           PRGRM_CD            CHAR(6) NOT NULL WITH DEFAULT,
           ACTVY_CD            CHAR(6) NOT NULL WITH DEFAULT,
           LCTN_CD             CHAR(6) NOT NULL WITH DEFAULT,
           RULE_CLS_CD         CHAR(4) NOT NULL WITH DEFAULT,
           FSCL_YR             CHAR(2) NOT NULL WITH DEFAULT,
           PSTNG_PRD           CHAR(2) NOT NULL WITH DEFAULT,
           NSF_OVRDE           CHAR(1) NOT NULL WITH DEFAULT,
           LIEN_CLOSED_CD      CHAR(1) NOT NULL WITH DEFAULT,
           GOV_OWNED_IND       CHAR(1) NOT NULL WITH DEFAULT,
           FK_RQD_RQST_CD      CHAR(8) NOT NULL,
           FK_RQD_ITEM_NBR     DECIMAL(4) NOT NULL)
           END-EXEC.
       01  RQA-RQA-RQST-ACCT.
           05  RQA-USER-CD                    PIC X(08).
           05  RQA-LAST-ACTVY-DT              PIC X(05).
           05  RQA-SEQ-NBR                    PIC S9(04)         COMP-3.
           05  RQA-PCT-DSTBN                  PIC S9(03)V999     COMP-3.
           05  RQA-ACTVY-DT                   PIC X(10).
           05  RQA-ACCT-AMT                   PIC S9(10)V99      COMP-3.
           05  RQA-ACCT-ERROR-IND             PIC X(01).
           05  RQA-COA-CD                     PIC X(01).
           05  RQA-ACCT-INDX-CD               PIC X(10).
           05  RQA-FUND-CD                    PIC X(06).
           05  RQA-ORGN-CD                    PIC X(06).
           05  RQA-ACCT-CD                    PIC X(06).
           05  RQA-PRGRM-CD                   PIC X(06).
           05  RQA-ACTVY-CD                   PIC X(06).
           05  RQA-LCTN-CD                    PIC X(06).
           05  RQA-RULE-CLS-CD                PIC X(04).
           05  RQA-FSCL-YR                    PIC X(02).
           05  RQA-PSTNG-PRD                  PIC X(02).
           05  RQA-NSF-OVRDE                  PIC X(01).
           05  RQA-LIEN-CLOSED-CD             PIC X(01).
           05  RQA-GOV-OWNED-IND              PIC X(01).
           05  RQA-FK-RQD-RQST-CD             PIC X(08).
           05  RQA-FK-RQD-ITEM-NBR            PIC S9(04)         COMP-3.
