
           EXEC SQL DECLARE CCT_COST_CONTR_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           CNTRB_PCT           DECIMAL(6,3) NOT NULL WITH DEFAULT,
           ACCT_INDX_CD        CHAR(10) NOT NULL WITH DEFAULT,
           FUND_CD             CHAR(6) NOT NULL WITH DEFAULT,
           ORGN_CD             CHAR(6) NOT NULL WITH DEFAULT,
           ACCT_CD             CHAR(6) NOT NULL WITH DEFAULT,
           PRGRM_CD            CHAR(6) NOT NULL WITH DEFAULT,
           ACTVY_CD            CHAR(6) NOT NULL WITH DEFAULT,
           LCTN_CD             CHAR(6) NOT NULL WITH DEFAULT,
           COST_SHR_CR_ACCT    CHAR(6) NOT NULL WITH DEFAULT,
           FK_CSE_COST_SHR_CD  CHAR(6) NOT NULL,
           FK_CSE_START_DT     DATE NOT NULL,
           FK_CSE_TIME_STMP    TIME NOT NULL,
           CSE_SET_TS          TIMESTAMP NOT NULL)
           END-EXEC.
       01  CCT-CCT-COST-CONTR.
           05  CCT-USER-CD                    PIC X(08).
           05  CCT-LAST-ACTVY-DT              PIC X(05).
           05  CCT-CNTRB-PCT                  PIC S9(03)V999     COMP-3.
           05  CCT-ACCT-INDX-CD               PIC X(10).
           05  CCT-FUND-CD                    PIC X(06).
           05  CCT-ORGN-CD                    PIC X(06).
           05  CCT-ACCT-CD                    PIC X(06).
           05  CCT-PRGRM-CD                   PIC X(06).
           05  CCT-ACTVY-CD                   PIC X(06).
           05  CCT-LCTN-CD                    PIC X(06).
           05  CCT-COST-SHR-CR-ACCT           PIC X(06).
           05  CCT-FK-CSE-COST-SHR-CD         PIC X(06).
           05  CCT-FK-CSE-START-DT            PIC X(10).
           05  CCT-FK-CSE-TIME-STMP           PIC X(8).
           05  CCT-CSE-SET-TS                 PIC X(26).
