      ******************************************************************
      * DCLGEN TABLE(CRG_CHARGE_CARD_V)                                *
      *        LIBRARY(FIST.IFIS.SRCLIB(HVCRG))                        *
      *        ACTION(REPLACE)                                         *
      *        LANGUAGE(COBOL)                                         *
      *        NAMES(CRG-)                                             *
      *        STRUCTURE(CRG-CRD-CHARGE-CARD)                          *
      *        QUOTE                                                   *
      *        COLSUFFIX(YES)                                          *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE CRG_CHARGE_CARD_V TABLE
           ( USER_CD                        CHAR(8) NOT NULL,
             LAST_ACTVY_DT                  CHAR(5) NOT NULL,
             OWNER_IREF_ID                  DECIMAL(7, 0) NOT NULL,
             OWNER_ENTPSN_IND               CHAR(1) NOT NULL,
             CARD_TYP                       CHAR(4) NOT NULL,
             CARD_NBR                       CHAR(18) NOT NULL,
             EXPRN_DT                       CHAR(4) NOT NULL,
             OWNER_ACCT_ONE                 CHAR(1) NOT NULL,
             OWNER_ACCT_NINE                CHAR(9) NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE  CRG_CHARGE_CARD_V                 *
      ******************************************************************
       01  CRG-CRD-CHARGE-CARD.
           10 CRG-USER-CD              PIC X(8).
           10 CRG-LAST-ACTVY-DT        PIC X(5).
           10 CRG-OWNER-IREF-ID        PIC S9(7)V USAGE COMP-3.
           10 CRG-OWNER-ENTPSN-IND     PIC X(1).
           10 CRG-CARD-TYP             PIC X(4).
           10 CRG-CARD-NBR             PIC X(18).
           10 CRG-EXPRN-DT             PIC X(4).
           10 CRG-OWNER-ACCT-ONE       PIC X(1).
           10 CRG-OWNER-ACCT-NINE      PIC X(9).
