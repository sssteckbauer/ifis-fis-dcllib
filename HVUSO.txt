
           EXEC SQL DECLARE USO_USER_ORGN_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           COA_CD              CHAR(1) NOT NULL WITH DEFAULT,
           ACTVY_DT            DATE NOT NULL WITH DEFAULT,
           NSF_OVRDE           CHAR(1) NOT NULL WITH DEFAULT,
           TLRNC_PCTG          DECIMAL(6,3) NOT NULL WITH DEFAULT,
           TLRNC_AMT           DECIMAL(12,2) NOT NULL WITH DEFAULT,
           ORGN_CD             CHAR(6) NOT NULL,
           ORGN_TYP_CD         CHAR(2) NOT NULL,
           BUYER_CD            CHAR(4) NOT NULL WITH DEFAULT,
           FK_UPR_USER_ID      CHAR(8) NOT NULL)
           END-EXEC.
       01  USO-USO-USER-ORGN.
           05  USO-USER-CD                    PIC X(08).
           05  USO-LAST-ACTVY-DT              PIC X(05).
           05  USO-COA-CD                     PIC X(01).
           05  USO-ACTVY-DT                   PIC X(10).
           05  USO-NSF-OVRDE                  PIC X(01).
           05  USO-TLRNC-PCTG                 PIC S9(03)V999     COMP-3.
           05  USO-TLRNC-AMT                  PIC S9(10)V99      COMP-3.
           05  USO-ORGN-CD                    PIC X(06).
           05  USO-ORGN-TYP-CD                PIC X(02).
           05  USO-BUYER-CD                   PIC X(04).
           05  USO-FK-UPR-USER-ID             PIC X(08).
