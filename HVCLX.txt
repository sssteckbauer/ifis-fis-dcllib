
           EXEC SQL DECLARE CLX_CLAUSE_TXT_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           CLAUSE_TEXT         CHAR(55) NOT NULL WITH DEFAULT,
           FK_CLA_CLAUSE_CD    CHAR(8) NOT NULL,
           CLA_SET_TS          TIMESTAMP NOT NULL)
           END-EXEC.
       01  CLX-CLX-CLAUSE-TXT.
           05  CLX-USER-CD                    PIC X(08).
           05  CLX-LAST-ACTVY-DT              PIC X(05).
           05  CLX-CLAUSE-TEXT                PIC X(55).
           05  CLX-FK-CLA-CLAUSE-CD           PIC X(08).
           05  CLX-CLA-SET-TS                 PIC X(26).
