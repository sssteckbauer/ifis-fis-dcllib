
           EXEC SQL DECLARE CPS_TXT_SEQ_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           TEXT_CD_SEQ_ID      DECIMAL(3) NOT NULL,
           PLUS_DAYS           DECIMAL(3) NOT NULL WITH DEFAULT,
           DIFF_BATCH_FLAG     CHAR(1) NOT NULL WITH DEFAULT,
           SAME_BATCH_FLAG     CHAR(1) NOT NULL WITH DEFAULT,
           DIFF_BATCH_MAX      DECIMAL(3) NOT NULL WITH DEFAULT,
           FK_CPL_COMM_VIEW    CHAR(8) NOT NULL,
           FK_CPL_COMM_TYP     CHAR(4) NOT NULL,
           FK_CPL_COMM_PLNCD   CHAR(4) NOT NULL)
           END-EXEC.
       01  CPS-CPS-TXT-SEQ.
           05  CPS-USER-CD                    PIC X(08).
           05  CPS-LAST-ACTVY-DT              PIC X(05).
           05  CPS-TEXT-CD-SEQ-ID             PIC S9(03)         COMP-3.
           05  CPS-PLUS-DAYS                  PIC S9(03)         COMP-3.
           05  CPS-DIFF-BATCH-FLAG            PIC X(01).
           05  CPS-SAME-BATCH-FLAG            PIC X(01).
           05  CPS-DIFF-BATCH-MAX             PIC S9(03)         COMP-3.
           05  CPS-FK-CPL-COMM-VIEW           PIC X(08).
           05  CPS-FK-CPL-COMM-TYP            PIC X(04).
           05  CPS-FK-CPL-COMM-PLNCD          PIC X(04).
