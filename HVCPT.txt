
           EXEC SQL DECLARE CPT_PAR_TXT_V TABLE
           (USER_CD            CHAR(8) NOT NULL WITH DEFAULT,
           LAST_ACTVY_DT       CHAR(5) NOT NULL WITH DEFAULT,
           PARA_TEXT           CHAR(79) NOT NULL WITH DEFAULT,
           FK_CST_PARA_CD      CHAR(4) NOT NULL,
           CST_SET_TS          TIMESTAMP NOT NULL)
           END-EXEC.
       01  CPT-CPT-PAR-TXT.
           05  CPT-USER-CD                    PIC X(08).
           05  CPT-LAST-ACTVY-DT              PIC X(05).
           05  CPT-PARA-TEXT                  PIC X(79).
           05  CPT-FK-CST-PARA-CD             PIC X(04).
           05  CPT-CST-SET-TS                 PIC X(26).
