
           EXEC SQL DECLARE PFD_PFLD_DATA TABLE
           (NEXT_DEV           DECIMAL(8) NOT NULL WITH DEFAULT,
           LIT_POOL_OFFST      DECIMAL(8) NOT NULL WITH DEFAULT,
           LIT_POOL_LEN        DECIMAL(4) NOT NULL WITH DEFAULT,
           DEV_FLAG            CHAR(8) NOT NULL WITH DEFAULT,
           DEV_ENTRY_FLAG      CHAR(8) NOT NULL WITH DEFAULT,
           ATTR_FLAG           CHAR(8) NOT NULL WITH DEFAULT,
           OUTLINE_FLAG        CHAR(8) NOT NULL WITH DEFAULT,
           EXT_HGLT_FLAG       CHAR(8) NOT NULL WITH DEFAULT,
           EXT_COLOR_FLAG      CHAR(8) NOT NULL WITH DEFAULT,
           PFLD_INFO           CHAR(240) NOT NULL WITH DEFAULT,
           FK_PLF_PKEY_TS      TIMESTAMP NOT NULL,
           PLF_TS              TIMESTAMP NOT NULL)
           END-EXEC.
       01  PFD-PFD-PFLD-DATA.
           05  PFD-NEXT-DEV                   PIC S9(8)          COMP-3.
           05  PFD-LIT-POOL-OFFST             PIC S9(8)          COMP-3.
           05  PFD-LIT-POOL-LEN               PIC S9(4)          COMP-3.
FCSAH *    05  PFD-DEV-FLAG                   BIT X(08).
           05  PFD-DEV-FLAG                   PIC X(08).
FCSAH *    05  PFD-DEV-ENTRY-FLAG             BIT X(08).
           05  PFD-DEV-ENTRY-FLAG             PIC X(08).
FCSAH *    05  PFD-ATTR-FLAG                  BIT X(08).
           05  PFD-ATTR-FLAG                  PIC X(08).
FCSAH *    05  PFD-OUTLINE-FLAG               BIT X(08).
           05  PFD-OUTLINE-FLAG               PIC X(08).
FCSAH *    05  PFD-EXT-HGLT-FLAG              BIT X(08).
           05  PFD-EXT-HGLT-FLAG              PIC X(08).
FCSAH *    05  PFD-EXT-COLOR-FLAG             BIT X(08).
           05  PFD-EXT-COLOR-FLAG             PIC X(08).
           05  PFD-PFLD-INFO                  PIC X(240).
           05  PFD-FK-PLF-PKEY-TS             PIC X(26).
           05  PFD-PLF-TS                     PIC X(26).
