      ******************************************************************
      * DCLGEN TABLE(GLL_GNRL_LDGR_V)                                  *
      *        LIBRARY(FISQ.IFIS.SRCLIB(HVGLL))                        *
      *        ACTION(REPLACE)                                         *
      *        LANGUAGE(COBOL)                                         *
      *        NAMES(GLL-)                                             *
      *        STRUCTURE(GLL-GLL-GNRL-LDGR)                            *
      *        QUOTE                                                   *
      *        COLSUFFIX(YES)                                          *
      * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
      ******************************************************************
           EXEC SQL DECLARE GLL_GNRL_LDGR_V TABLE
           ( USER_CD                        CHAR(8) NOT NULL,
             LAST_ACTVY_DT                  CHAR(5) NOT NULL,
             SYSTEM_TIME_STMP               CHAR(8) NOT NULL,
             UNVRS_CD                       CHAR(2) NOT NULL,
             COA_CD                         CHAR(1) NOT NULL,
             FSCL_CC                        CHAR(2) NOT NULL,
             FSCL_YR                        CHAR(2) NOT NULL,
             FUND_CD                        CHAR(6) NOT NULL,
             ACCT_CD                        CHAR(6) NOT NULL,
             ACTVY_DT                       DATE NOT NULL,
             SMRY_PDC_DR_01                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_DR_02                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_DR_03                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_DR_04                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_DR_05                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_DR_06                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_DR_07                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_DR_08                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_DR_09                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_DR_10                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_DR_11                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_DR_12                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_DR_13                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_DR_14                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_CR_01                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_CR_02                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_CR_03                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_CR_04                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_CR_05                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_CR_06                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_CR_07                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_CR_08                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_CR_09                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_CR_10                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_CR_11                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_CR_12                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_CR_13                 DECIMAL(15, 2) NOT NULL,
             SMRY_PDC_CR_14                 DECIMAL(15, 2) NOT NULL,
             BEG_SMRY_PDC_DR                DECIMAL(15, 2) NOT NULL,
             BEG_SMRY_PDC_CR                DECIMAL(15, 2) NOT NULL,
             DW_GL_ID                       INTEGER NOT NULL
           ) END-EXEC.
      ******************************************************************
      * COBOL DECLARATION FOR TABLE GLL_GNRL_LDGR_V                    *
      ******************************************************************
       01  GLL-GLL-GNRL-LDGR.
           10 GLL-USER-CD          PIC X(8).
           10 GLL-LAST-ACTVY-DT    PIC X(5).
           10 GLL-SYSTEM-TIME-STMP  PIC X(8).
           10 GLL-UNVRS-CD         PIC X(2).
           10 GLL-COA-CD           PIC X(1).
           10 GLL-FSCL-CC          PIC X(2).
           10 GLL-FSCL-YR          PIC X(2).
           10 GLL-FUND-CD          PIC X(6).
           10 GLL-ACCT-CD          PIC X(6).
           10 GLL-ACTVY-DT         PIC X(10).
           10 GLL-SMRY-PDC-DR-01   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-DR-02   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-DR-03   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-DR-04   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-DR-05   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-DR-06   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-DR-07   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-DR-08   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-DR-09   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-DR-10   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-DR-11   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-DR-12   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-DR-13   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-DR-14   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-CR-01   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-CR-02   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-CR-03   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-CR-04   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-CR-05   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-CR-06   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-CR-07   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-CR-08   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-CR-09   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-CR-10   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-CR-11   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-CR-12   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-CR-13   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-SMRY-PDC-CR-14   PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-BEG-SMRY-PDC-DR  PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-BEG-SMRY-PDC-CR  PIC S9(13)V9(2) USAGE COMP-3.
           10 GLL-DW-GL-ID         PIC S9(9) USAGE COMP.
      ******************************************************************
      * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 41      *
      ******************************************************************
